import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Header from './components/layouts/Header'
import Content from './components/layouts/Content'
import TableData from './components/layouts/tableData'

function App() {
  return (<div>
    <Header/>
    <p style={{fontWeight:'bold', fontSize:'small'}}>Serviceable in HARDOI & FARRUKHABAD!</p>
    <Content/>
    <p style={{marginTop:'20px'}}></p>
    <TableData/>
    </div>
  );
}

export default App;
